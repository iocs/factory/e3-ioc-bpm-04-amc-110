require essioc
require adsis8300bpm

# set macros
epicsEnvSet("CONTROL_GROUP",  "PBI-BPM04")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-110")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-6")
epicsEnvSet("EVR_NAME",       "PBI-BPM04:Ctrl-EVR-101:")
epicsEnvSet("SYSTEM1_PREFIX", "MBL-010LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM1_NAME",   "MBL 010 LWU BPM 01")
epicsEnvSet("SYSTEM2_PREFIX", "MBL-020LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM2_NAME",   "MBL 020 LWU BPM 01")

# load BPM
iocshLoad("$(adsis8300bpm_DIR)/bpm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

